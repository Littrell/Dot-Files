" autocmd {{{
    " Source .vimrc on save
    autocmd! BufWritePost ~/.vimrc nested :source ~/.vimrc

    " python doesn't like tabs
    autocmd FileType py set expandtab

    " Redraw fix
    autocmd GuiEnter * set background&

    " Disable automatically adding comments on new lines; shit's annoying
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

    " Remove trailing whitespace per filetype {{{
        autocmd BufWritePre *.js :%s/\s\+$//e
        autocmd BufWritePre *.jsx :%s/\s\+$//e
        autocmd BufWritePre *.scss :%s/\s\+$//e
        autocmd BufWritePre *.html :%s/\s\+$//e
        autocmd BufWritePre *.php :%s/\s\+$//e
        autocmd BufWritePre *.go :%s/\s\+$//e
        autocmd BufWritePre *.ts :%s/\s\+$//e
    " }}}

    " Rename tmux pane based on which file is open {{{
        autocmd BufEnter * call system("tmux rename-window " . expand("%:t"))
        autocmd VimLeave * call system("tmux rename-window zsh")
        autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
    " }}}


    " Force rescan buffer when highlighting {{{
        autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
        autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear
    " }}}

"}}}

" Vundle {{{
    filetype off
    set runtimepath+=~/vundle/Vundle.vim
    call vundle#begin()

    " ViM plugin manager
    Bundle 'VundleVim/Vundle.vim'

    " A good looking theme that's easy on the eyes
    Bundle 'Lokaltog/vim-distinguished'

    " Automatically close quotes, parens, etc
    Bundle 'jiangmiao/auto-pairs'

    " Zebra striped indent guides
    Bundle 'nathanaelkane/vim-indent-guides'

    " ViM fuzzy finding
    set rtp+=~/.fzf
    Bundle 'junegunn/fzf.vim'

    " ViM mark navigation plugin
    Bundle 'kshenoy/vim-signature'

    " Git
    Bundle 'tpope/vim-fugitive'
    Bundle 'airblade/vim-gitgutter'

    " Airline
    Bundle 'vim-airline/vim-airline'
    Bundle 'vim-airline/vim-airline-themes'
    Bundle 'powerline/fonts'

    " Cleans up JSON for easy reading
    Bundle 'elzr/vim-json'

    " JavaScript syntax helper
    Bundle 'pangloss/vim-javascript'

    " Docker syntax highlighting
    Bundle 'ekalinin/Dockerfile.vim'

    " YAML syntax helper
    Bundle 'stephpy/vim-yaml'

    " CSS3 syntax highlighting
    Bundle 'hail2u/vim-css3-syntax'

    " Auto-close quotes, parens, etc
    Bundle 'tpope/vim-surround'

    " Auto closing HTML tags
    Bundle 'alvan/vim-closetag'

    " Terraform
    Bundle 'hashivim/vim-terraform'

    " Markdown
    Bundle 'godlygeek/tabular'
    Bundle 'plasticboy/vim-markdown'

    " Prerequisites for :Gbrowse
    Bundle 'tpope/vim-rhubarb'

    " Colorize based on color codes
    Bundle 'vim-scripts/AnsiEsc.vim'

    call vundle#end()
    filetype plugin indent on
" }}}

" -------------------------------------------------
" Configs
" -------------------------------------------------
" Basic configs {{{
    set encoding=utf8
    set incsearch              " incremental highlighting
    set hlsearch               " highlight cursor
    set laststatus=2
    set matchpairs=(:),{:},[:] " Define characters for 'showmatch'
    set noswapfile             " turn off swap files
    set number
    set nowrap
    set nocompatible           " Ensure enhancements are turned on
    set re=1                   " force ViM to use new regex engine
    set showmatch              " enable showing matching enclosing characters
    set t_Co=256               " 256 color mode, g
    set t_ut=                  " Fixes the background color in vim when using tmux
    set title
    set textwidth=0            " Disable 'textwidth'
    set foldmethod=marker      " Turn on code folding by marker
    set autoread               " automatically re-read files if unmodified inside vim
    set nostartofline          " leave cursor position alone
    set scrolloff=5            " keep at least 5 offsets around the cursor
    set sidescrolloff=5        " keep at least 5 offsets around the cursor
" }}}

" Remaps {{{
    " move screen with cursor when not using arrow keys
    noremap j j<c-e>

    map s "_d
" }}}

" ViM speed improvements {{{
    set timeoutlen=1000 ttimeoutlen=0
    set ttyfast                         " improves vim scrolling and redraws
    set lazyredraw                      " buffer screen updates instead of updating all the time
" }}}

" Spell Checker {{{
    set spelllang=en
    set spellfile=$HOME/Dot-Files/en.utf-8.add
" }}}

" Mouse support {{{
    set mouse=a                         " enable mouse in all modes
    set ttymouse=xterm2                 " set this to name of your terminal that supports mouse codes
" }}}

" Advanced File Explorer {{{
    let g:netrw_banner = 0
    let g:netrw_liststyle = 3
    let g:netrw_browse_split = 0
    let g:netrw_altv = 1
    let g:netrw_winsize = 25
" }}}

" -------------------------------------------------
" Syntax highlighting & formatting
"--------------------------------------------------
" Basic syntax configs {{{
    " Turn on syntax highlighting
    syntax on

    " set colorscheme
    silent! colorscheme darkblue
    silent! colorscheme distinguished

    " Custom highlighting {{{
        highlight TrailingWhiteSpace ctermbg=red ctermfg=white
        highlight Note ctermbg=green ctermfg=white
        highlight ConsoleLog ctermbg=red ctermfg=white
        highlight Scaffolding ctermbg=blue ctermfg=white
        match TrailingWhiteSpace / \+$/
        match Note / NOTE/
        match ConsoleLog / console\.log/
        match Scaffolding / SCAFFOLDING/
    " }}}

    " nginx syntax highlighting
    au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif

    " extra syntax application
    au BufNewFile,BufRead *.j set filetype=objj
" }}}

" Tabs and indenting {{{
    set tabstop=2                           " A tab is four spaces
    set smarttab                            " <TAB> key inserts indentation according to 'shiftwidth'
    set softtabstop=2                       " When hitting <BS>, pretend like a tab is removed, even if spaces
    set expandtab                           " Insert space characters whenever the tab key is pressed
    set shiftwidth=2                        " Number of spaces to use for autoindenting
    set shiftround                          " Use multiple of shiftwidth when indenting with '<' and '>'
    set backspace=indent,eol,start          " Define how backspacing works in insert mode
    set list                                " Make whitespace characters visible
    set listchars=tab:➝\ ,extends:#,nbsp:.  " Define whitespace characters
    set autoindent                          " turn on
    set cindent                             " stricter rules for C
" }}}

" {{{ Indent guide config
    let g:indent_guides_enable_on_vim_startup = 1
    let g:indent_guides_auto_colors = 0
    autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd guibg=darkgrey ctermbg=234
    autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=lightgrey ctermbg=235
" }}}

" Airline configuration {{{
    let g:airline_powerline_fonts = 1            " Automatically populate 'g:airline_symbols'
    let g:airline#extensions#tabline#enabled = 1 " Display all buffers as tabline
" }}}

" Final configurations {{{
    set secure " harden vim
" }}}
