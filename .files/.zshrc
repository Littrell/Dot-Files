# OS {{{
    export IS_MAC=false
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        export IS_MAC=true
    elif [[ "$OSTYPE" == "cygwin" ]]; then
    elif [[ "$OSTYPE" == "msys" ]]; then
    elif [[ "$OSTYPE" == "win32" ]]; then
    elif [[ "$OSTYPE" == "freebsd"* ]]; then
    else
    fi
# }}}

# Paths/Exports {{{
    export GOPATH=$HOME/go
    if $IS_MAC; then
      export GOROOT=/usr/local/go
      export VSCODEPATH=/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin
      export BREWPATH=/opt/homebrew/bin
      export PYBIN=~/Library/Python/3.8/bin
    else
      export GOROOT=/usr/local/go
      export VSCODEPATH=
      export BREWPATH=
      export PYBIN=
    fi
    export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/bin:$GOPATH/bin:$GOROOT/bin:$VSCODEPATH:$BREWPATH:$PYBIN:$PATH

    # cdpath
    export CDPATHS=${HOME}/repos
    setopt auto_cd
    cdpath=(. $HOME $CDPATHS)  # Set this to (.) if you only want directory completion from the path.
    typeset -U path cdpath fpath
# }}}

# Alias {{{
    alias zshrc="$EDITOR $HOME/.zshrc"
    alias vimrc="$EDITOR $HOME/.vimrc"

    # Basic alias'
    alias ls="ls -lahFG"
    alias cp="rsync -a --stats --progress"
    alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
    alias tardir="tar -zcvf"
    alias oldestfile="find . -type f -printf '%T+ %p\n' | sort | head -n 1"

    # What's the weather
    alias weather="curl wttr.in"

    # Find alias'
    alias fid="find . -type d -name"
    alias fif="find . -type f -name"

    # Docker
    alias d="docker"
    alias dps='docker ps --format "table {{.ID}}\t{{.Image}}\t{{.Names}}"'
    alias dc="docker compose"

    # This overwrites the gst installed
    # by Antigen. Using `git st` because it
    # includes a `git log -1 --stat`
    alias gst="git st"

    # A quick function for when I'm done a project and
    # want to clean up after a merge.
    function git-done() {
        main_branch=$(git symbolic-ref refs/remotes/origin/HEAD | cut -d'/' -f4)
        branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')
        git checkout $main_branch && git pull && git branch -D $branch
    }

    # ls when cd
    function chpwd() {
        emulate -L zsh
        if $IS_MAC; then
            ls -lahFG
        else
            ls -lahF --color=auto
        fi
    }

    function oldfiles() {
        git ls-tree -r --name-only HEAD | while read filename; do
            echo "$(git log -1 --format="%at | %h | %an | %ad |" -- $filename) $filename"
        done
    }
# }}}

# Antigen {{{
    source $HOME/Dot-Files/antigen/antigen.zsh
    antigen use oh-my-zsh
    antigen bundle git
    antigen bundle lein
    antigen bundle command-not-found

    # Better 256 color support
    antigen bundle chrissicool/zsh-256color

    antigen theme denysdovhan/spaceship-prompt
    export SPACESHIP_DOCKER_SHOW=false

    # Set theme to my current favorite
    #antigen theme bhilburn/powerlevel9k powerlevel9k

    # Auto close "", '', {} etc
    antigen bundle hlissner/zsh-autopair

    # Go up a few directories...
    # cd ..          => up
    # cd ../..       => up 2
    # cd ../../../.. => up 4
    antigen bundle peterhurford/up.zsh

    # Make the following much simpler:
    # git add .
    # git commit -m 'some message'
    # git pull origin some_branch
    # git push origin some_branch
    #
    # Instead:
    # send 'some message'
    antigen bundle robertzk/send.zsh

    # Syntax highlighting bundle.
    antigen bundle zdharma/fast-syntax-highlighting
    #antigen bundle zsh-users/zsh-syntax-highlighting

    # Auto update Antigen like oh-my-zsh
    antigen bundle unixorn/autoupdate-antigen.zshplugin

    # Temorarily commented out because it causes issues
    # on macOS
    #antigen bundle b4b4r07/enhancd

    antigen apply
# }}}

# {{{ More exports
    export HISTFILE="${HOME}/.zsh_history"
    export HISTSIZE=1000000
    export SAVEHIST=1000000
    export LESSHISTFILE="-"
    export PAGER="less"
    export READNULLCMD="${PAGER}"
    export BROWSER="chrome"
    export EDITOR="vim"
    export VIRTUAL_ENV_DISABLE_PROMPT=1
    export LD_LIBRARY_PATH=/usr/local/lib:/opt/local/lib
    export C_INCLUDE_PATH=/usr/local/include:/opt/local/include
    export LIBRARY_PATH=/opt/local/lib
    [ -n "$TMUX" ] && export TERM=screen-256color
# }}}

# {{{ Colorize man pages
    export LESS_TERMCAP_mb=$'\E[01;31m'   # begin blinking
    export LESS_TERMCAP_md=$'\E[01;31m'   # begin bold
    export LESS_TERMCAP_me=$'\E[0m'       # end mode
    export LESS_TERMCAP_se=$'\E[0m'       # end standout-mode
    export LESS_TERMCAP_so=$'\E[1;33;40m' # begin standout-mode - info box
    export LESS_TERMCAP_ue=$'\E[0m'       # end underline
    export LESS_TERMCAP_us=$'\E[1;32m'    # begin underline
# }}}

# {{{ ZSH settings
    setopt nohup
    setopt autocd
    setopt cdablevars
    setopt nobgnice
    setopt noclobber
    setopt shwordsplit
    setopt interactivecomments
    setopt autopushd pushdminus pushdsilent pushdtohome
    setopt histreduceblanks histignorespace inc_append_history
    setopt nobeep

    # keybindings
    bindkey -e # emacs
    bindkey "\e[A" up-line-or-search
    bindkey "\e[B" down-line-or-search

    # Prompt requirements
    setopt extended_glob prompt_subst
    autoload colors zsh/terminfo

    # New style completion system
    autoload -U compinit; compinit
    #  * List of completers to use
    zstyle ":completion:*" completer _complete _match _approximate
    #  * Allow approximate
    zstyle ":completion:*:match:*" original only
    zstyle ":completion:*:approximate:*" max-errors 1 numeric
    #  * Selection prompt as menu
    zstyle ":completion:*" menu select=1
    #  * Menu selection for PID completion
    zstyle ":completion:*:*:kill:*" menu yes select
    zstyle ":completion:*:kill:*" force-list always
    zstyle ":completion:*:processes" command "ps -au$USER"
    zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;32"
    #  * Don't select parent dir on cd
    zstyle ":completion:*:cd:*" ignore-parents parent pwd
    #  * Complete with colors
    zstyle ":completion:*" list-colors ""
    # Ignore user home direcotry completion
    zstyle ':completion:*:*:*:users' ignored-patterns '*'

    # vcs_info
    autoload -Uz vcs_info
    zstyle ':vcs_info:*' stagedstr '%F{28}●'
    zstyle ':vcs_info:*' unstagedstr '%F{11}●'
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:[svn]' format '[%c %b%u]'
    zstyle ':vcs_info:*' enable bzr git svn
# }}}

# {{{ NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh"  ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion"  ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path"  ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")
    if [ "$nvmrc_node_version" = "N/A"  ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version"  ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)"  ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
# }}}

# Gets Docker to play nice with macOS
if $IS_MAC; then
    unset DOCKER_HOST
    unset DOCKER_TLS_VERIFY
    unset DOCKER_TLS_PATH
fi

# Init zoxide
eval "$(zoxide init zsh)"

# Init fuck
eval $(thefuck --alias)

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

