#!/usr/bin/env bash

##############################
### Required Installations ###
##############################

Required=(
    'git'
    'tput'
    'vim'
    'zsh'
)

tput setaf 1 # red
for requiree in "${Required[@]}"; do
    command -v $requiree >/dev/null 2>&1 || {
        echo >&2 "${requiree} is a required installation, exiting..."
        exit 1
    }
done
tput setaf 7 # white

#########################
### Symlink dot files ###
#########################

Files=('.gitconfig' '.tmux.conf' '.vimrc' '.zshrc')

for file in "${Files[@]}"; do
    rm $HOME/$file
    ln -s $HOME/Dot-Files/.files/$file $HOME/$file
done

#####################
### Installations ###
#####################

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
$HOME/.fzf/install

# Vundle.vim
if [[ ! -d $HOME/vundle/Vundle.vim ]]; then
    git clone https://github.com/gmarik/Vundle.vim.git $HOME/vundle/Vundle.vim
fi

# Install ViM plugins
vim +PluginInstall +qall

# Antigen
if [[ ! -d $HOME/Dot-Files/antigen ]]; then
    git clone https://github.com/zsh-users/antigen.git $HOME/Dot-Files/antigen
fi

###############################
### Suggested Installations ###
###############################

Suggestions=(
    'ag'
    'htop'
    'iotop'
    'roamer'
    'termsaver'
    'tmux'
)

tput setaf 3 # yellow
for suggestion in "${Suggestions[@]}"; do
    command -v $sugestion >/dev/null 2>&1 || {
        echo >&2 "${suggestion} is a suggested installation, continuing..."
    }
done
tput setaf 7 # white
