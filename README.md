```bash
git clone git@gitlab.com:Littrell/Dot-Files.git
bash $HOME/Dot-Files/setup.sh
```

## Other useful tools

### bat

...instead of `cat`

`sudo apt install -y bat`

`brew install bat`

### exa

...instead of `ls`

`sudo apt install -y exa`

`brew install exa`

### dust

...instead of `du`

`brew install dust`

### ytop

...instead of `top`

```bash
brew tap cjbassi/ytop
brew install ytop
```

### zoxide

...instead of `cd`

`sudo apt install -y zoxide`

`brew install zoxide`

### yank

`brew install yank`

Useful for copying command output to buffer.

Example:

```bash
bat somefiletoyankalinefrom.txt | yank -l
```

### glow

`brew install glow`

View markdown in the terminal

### fd

...instead of `find`

`sudo apt install -y fd-find`

`brew install fd`

