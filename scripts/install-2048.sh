#!/bin/bash

type gcc >/dev/null 2>&1 && {
    wget https://raw.githubusercontent.com/mevdschee/2048.c/master/2048.c
    gcc -o 2048 2048.c
    sudo mv 2048 /usr/local/bin
    rm 2048.c
}
